﻿<#@ template language="C#" hostspecific="True" #>
<#@ assembly name="System.Core" #>
<#@ assembly name="System.Xml" #>
<#@ assembly name="EnvDTE" #>
<#@ assembly name="Microsoft.VisualStudio.OLE.Interop" #>
<#@ assembly name="Microsoft.VisualStudio.Shell" #>
<#@ assembly name="Microsoft.VisualStudio.Shell.Interop" #>
<#@ assembly name="Microsoft.VisualStudio.Shell.Interop.8.0" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.Diagnostics" #>
<#@ import namespace="System.IO" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Xml" #>
<#@ import namespace="System.Xml.Serialization" #>
<#@ import namespace="Microsoft.VisualStudio.Shell" #>
<#@ import namespace="Microsoft.VisualStudio.Shell.Interop" #>
<#@ import namespace="Microsoft.VisualStudio.TextTemplating" #>
<#
// The stream used to read the settings file.
StreamReader settingsReader = null;

try {
	// Inizialation of the variables which depend on the host.
	Initialize(Host.ResolvePath);

	// We read our settings from the XML file.
	settingsReader = new StreamReader(GpgenSettings);
	var mySettings = DeserializeFrom(settingsReader);

	// Scanner creation
	var scannerSettings = mySettings.scanner;
	if (scannerSettings.enabled) {
		var procInfo = new ProcessStartInfo() {
			Arguments = String.Format(GplexArgs, PathResolver(scannerSettings.input)),
			CreateNoWindow = true,
			FileName = GplexPath,
			RedirectStandardOutput = true,
			UseShellExecute = false
		};
		var gplex = Process.Start(procInfo);
		var lexer = gplex.StandardOutput.ReadToEnd();
		gplex.WaitForExit(ExecTimeout);
		// We need to replace the namespace with the one the user specified.
		lexer = lexer.Replace("LexScanner", scannerSettings.@namespace);
		// To allow code to be portable, we replace Console with a fake one.
		lexer = lexer.Replace("Console", "GardenPoints.T4.FakeConsole");
		GenerationEnvironment.Append(lexer);
		SaveOutput(GplexOutput);
	}

	// Parser creation
	var parserSettings = mySettings.parser;
	if (parserSettings.enabled) {
        var procInfo = new ProcessStartInfo() {
			Arguments = String.Format(GppgArgs, PathResolver(parserSettings.input)),
			CreateNoWindow = true,
			FileName = GppgPath,
			RedirectStandardOutput = true,
			UseShellExecute = false
		};
		var gppg = Process.Start(procInfo);
		var parser = gppg.StandardOutput.ReadToEnd();
		gppg.WaitForExit(ExecTimeout);
		// We need to replace the namespace with the one the user specified.
		parser = parser.Replace("LexScanner", parserSettings.@namespace);
		// To allow code to be portable, we replace Console with a fake one.
		parser = parser.Replace("System.Console", "Console");
		parser = parser.Replace("Console", "GardenPoints.T4.FakeConsole");
        // And we need to deal with missing IO functionalities.
        const string replace = "PCLStorage.FileSystem.Current.LocalStorage.GetFileAsync(args[0]).Result.OpenAsync(PCLStorage.FileAccess.Read).Result";
        parser = parser.Replace("args[0]", replace);
		GenerationEnvironment.Append(parser);
		SaveOutput(GppgOutput);
	}
} finally {
	if (settingsReader != null) {
		settingsReader.Close();
	}	
	DeleteOldOutputs();
}
#>
<#+
	// Error messages
	const string invalidElement = "Invalid element declared inside settings file: {0}";
	const string wrongFormat = "Given settings file has a wrong format";
	const string valueExpected = "Expected text value, found: {0}";

	// Executables timeout
	const int ExecTimeout = 1000;

	// Current path resolver
	static Func<string, string> PathResolver;

	// Settings file
	static string GpgenSettings; 

	// Garden Points executables and outputs
	const string GplexOutput = "Gpgen.Scanner.cs";
	const string GppgOutput = "Gpgen.Parser.cs";
	static readonly string GplexArgs = "/out:- /summary {0}";
	static readonly string GppgArgs = " {0}";
	static string GplexPath;
	static string GppgPath;

	static void Initialize(Func<string, string> pathResolver)
	{
		PathResolver = pathResolver;
		GpgenSettings = PathResolver("Gpgen.xml");
		GplexPath = pathResolver("Gplex.exe");
		GppgPath = pathResolver("Gppg.exe");
	}

	static settings DeserializeFrom(TextReader reader)
	{
		XmlSerializer serializer = new XmlSerializer(typeof(settings));
		return (settings)serializer.Deserialize(reader);
	}

	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
	[System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=false)]
	public partial class settings {
    
		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
		public settingsScanner scanner;
    
		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
		public settingsParser parser;
	}

	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
	public partial class settingsScanner {
    
		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
		public string input;
    
		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
		public string @namespace;
    
		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public bool enabled;
    
		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool enabledSpecified;
	}

	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
	public partial class settingsParser {
    
		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
		public string input;
    
		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
		public string @namespace;
    
		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public bool enabled;
    
		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool enabledSpecified;
	}
#>
<#+ 
	// Code taken from: http://www.olegsych.com/2008/03/how-to-generate-multiple-outputs-from-single-t4-template/	
    
	List<string> __savedOutputs = new List<string>();
    Engine __engine = new Engine();

    void DeleteOldOutputs()
    {
        EnvDTE.ProjectItem templateProjectItem = __getTemplateProjectItem();
        foreach (EnvDTE.ProjectItem childProjectItem in templateProjectItem.ProjectItems)
        {
            if (!__savedOutputs.Contains(childProjectItem.Name))
                childProjectItem.Delete();
        }
    }

    void ProcessTemplate(string templateFileName, string outputFileName)
    {
        string templateDirectory = Path.GetDirectoryName(Host.TemplateFile);
        string outputFilePath = Path.Combine(templateDirectory, outputFileName);

        string template = File.ReadAllText(Host.ResolvePath(templateFileName));
        string output = __engine.ProcessTemplate(template, Host);
        File.WriteAllText(outputFilePath, output);

        EnvDTE.ProjectItem templateProjectItem = __getTemplateProjectItem();
        templateProjectItem.ProjectItems.AddFromFile(outputFilePath);

        __savedOutputs.Add(outputFileName);
    }

    void SaveOutput(string outputFileName)
    {
        string templateDirectory = Path.GetDirectoryName(Host.TemplateFile);
        string outputFilePath = Path.Combine(templateDirectory, outputFileName);

        File.WriteAllText(outputFilePath, this.GenerationEnvironment.ToString());
        this.GenerationEnvironment = new StringBuilder();

        EnvDTE.ProjectItem templateProjectItem = __getTemplateProjectItem();
        templateProjectItem.ProjectItems.AddFromFile(outputFilePath);

        __savedOutputs.Add(outputFileName);
    }

    EnvDTE.ProjectItem __getTemplateProjectItem()
    {
        EnvDTE.Project dteProject = __getTemplateProject();

        IVsProject vsProject = __dteProjectToVsProject(dteProject);

        int iFound = 0;
        uint itemId = 0;
        VSDOCUMENTPRIORITY[] pdwPriority = new VSDOCUMENTPRIORITY[1];
        int result = vsProject.IsDocumentInProject(Host.TemplateFile, out iFound, pdwPriority, out itemId);
        if (result != VSConstants.S_OK)
            throw new Exception("Unexpected error calling IVsProject.IsDocumentInProject");
        if (iFound == 0)
            throw new Exception("Cannot retrieve ProjectItem for template file");
        if (itemId == 0)
            throw new Exception("Cannot retrieve ProjectItem for template file");

        Microsoft.VisualStudio.OLE.Interop.IServiceProvider itemContext = null;
        result = vsProject.GetItemContext(itemId, out itemContext);
        if (result != VSConstants.S_OK)
            throw new Exception("Unexpected error calling IVsProject.GetItemContext");
        if (itemContext == null)
            throw new Exception("IVsProject.GetItemContext returned null");
    
        ServiceProvider itemContextService = new ServiceProvider(itemContext);
        EnvDTE.ProjectItem templateItem = (EnvDTE.ProjectItem)itemContextService.GetService(typeof(EnvDTE.ProjectItem));
        Debug.Assert(templateItem != null, "itemContextService.GetService returned null");

        return templateItem;
    }

    EnvDTE.Project __getTemplateProject()
    {
        IServiceProvider hostServiceProvider = (IServiceProvider)Host;
        if (hostServiceProvider == null)
            throw new Exception("Host property returned unexpected value (null)");

        EnvDTE.DTE dte = (EnvDTE.DTE)hostServiceProvider.GetService(typeof(EnvDTE.DTE));
        if (dte == null)
            throw new Exception("Unable to retrieve EnvDTE.DTE");

        Array activeSolutionProjects = (Array)dte.ActiveSolutionProjects;
        if (activeSolutionProjects == null)
            throw new Exception("DTE.ActiveSolutionProjects returned null");

        EnvDTE.Project dteProject = (EnvDTE.Project)activeSolutionProjects.GetValue(0);
        if (dteProject == null)
            throw new Exception("DTE.ActiveSolutionProjects[0] returned null");

        return dteProject;
    }

    static IVsProject __dteProjectToVsProject(EnvDTE.Project project)
    {
        if (project == null) 
            throw new ArgumentNullException("project");
            
        string projectGuid = null;        

        // DTE does not expose the project GUID that exists at in the msbuild project file.        
        // Cannot use MSBuild object model because it uses a static instance of the Engine,         
        // and using the Project will cause it to be unloaded from the engine when the         
        // GC collects the variable that we declare.       
        using (XmlReader projectReader = XmlReader.Create(project.FileName))
        {
            projectReader.MoveToContent();
            object nodeName = projectReader.NameTable.Add("ProjectGuid");
            while (projectReader.Read())
            {
                if (Object.Equals(projectReader.LocalName, nodeName))
                {
                    projectGuid = (string)projectReader.ReadElementContentAsString(); 
                    break;
                }
            }
        }
        if (string.IsNullOrEmpty(projectGuid))
            throw new Exception("Unable to find ProjectGuid element in the project file");

        Microsoft.VisualStudio.OLE.Interop.IServiceProvider dteServiceProvider = 
            (Microsoft.VisualStudio.OLE.Interop.IServiceProvider)project.DTE;
        IServiceProvider serviceProvider = new ServiceProvider(dteServiceProvider); 
        IVsHierarchy vsHierarchy = VsShellUtilities.GetHierarchy(serviceProvider, new Guid(projectGuid));
            
        IVsProject vsProject = (IVsProject)vsHierarchy;
        if (vsProject == null)
            throw new ArgumentException("Project is not a VS project.");
        return vsProject;
    }
#>
