﻿//
// FakeConsole.cs
//
// Author:
//       Alessio Parma <alessio.parma@gmail.com>
//
// Copyright (c) 2014 Alessio Parma <alessio.parma@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.IO;
using System.Text;

namespace GardenPoints.T4
{  
    /// <summary>
    ///   Dummy class created to allow portability. Please do not use it.
    /// </summary>
    public static class FakeConsole
    {
        /// <summary>
        ///   Dummy field created to allow portability. Please do not use it.
        /// </summary>
        public static readonly FakeWriter Error = new FakeWriter();

        /// <summary>
        ///   Dummy field created to allow portability. Please do not use it.
        /// </summary>
        public static readonly FakeReader In = new FakeReader();

        /// <summary>
        ///   Dummy field created to allow portability. Please do not use it.
        /// </summary>
        public static readonly FakeWriter Out = new FakeWriter();
        
        /// <summary>
        ///   Dummy method created to allow portability. Please do not use it.
        /// </summary>
        public static void WriteLine(object obj)
        {
            // Dummy
        }       
    }

    /// <summary>
    ///   Dummy class created to allow portability. Please do not use it.
    /// </summary>
    public sealed class FakeReader : TextReader
    {
        
    }

    /// <summary>
    ///   Dummy class created to allow portability. Please do not use it.
    /// </summary>
    public sealed class FakeWriter : TextWriter
    {
        /// <summary>
        ///   Dummy method created to allow portability. Please do not use it.
        /// </summary>
        public override void Write(char value)
        {
            // Dummy
        }

        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }
}
